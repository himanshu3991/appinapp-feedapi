package com.appinapp.services;

import com.appinapp.models.PartnerConfigs;
import com.google.cloud.spanner.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.data.spanner.core.SpannerTemplate;

import java.util.List;

public class SpannerService {

    @Autowired
    SpannerTemplate spannerTemplate;

    public void getAllPartnersConfigs(){
        List<PartnerConfigs> partnersConfigs = this.spannerTemplate.readAll(PartnerConfigs.class);
        System.out.println("the size is "+partnersConfigs.size());
    }

}
