package com.appinapp.models;

import org.springframework.cloud.gcp.data.spanner.core.mapping.Column;
import org.springframework.cloud.gcp.data.spanner.core.mapping.PrimaryKey;
import org.springframework.cloud.gcp.data.spanner.core.mapping.Table;

@Table(name = "partnerconfigs")
public class PartnerConfigs {
    @PrimaryKey
    @Column(name = "partner_od")
    String partnerId;

    String partnerName;

    String partnerType;
}
