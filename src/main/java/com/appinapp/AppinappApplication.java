package com.appinapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.data.spanner.repository.config.EnableSpannerRepositories;

@EnableSpannerRepositories
@SpringBootApplication
public class AppinappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppinappApplication.class, args);
	}

}
